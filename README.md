# Gozem

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.


## Development server

 - install angular with:
    
        npm install -g @angular/cli

 - clone project with 

        git clone https://gitlab.com/Adanhounme/gozem.git

 - install project module with:

        npm install 

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


