# ### STAGE 1: Build ###
# FROM node:12.7-alpine AS build
# WORKDIR /usr/src/app
# COPY package.json ./
# # RUN npm install -g npm@6.13
# RUN npm install
# COPY . .
# RUN npm run build
# ### STAGE 2: Run ###
# FROM nginx:1.17.1-alpine
# COPY --from=build /usr/src/app/dist/ /usr/share/nginx/html

# FROM node:latest
# WORKDIR /usr/src/app
# COPY package.json ./
# RUN npm install
# RUN npm install pm2 -g
# COPY . .
# RUN npm run build
# #RUN npm install --save sequelize@next
# #RUN npm run prod 
# EXPOSE 4200
# CMD ["node", "server.js"]
# #CMD ["pm2-runtime", "server.js"]



### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:12-alpine as builder
COPY package.json  ./
RUN npm install 
COPY package-lock.json  ./
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run ng build -- --prod --output-path=dist
### STAGE 2: Setup ###
FROM nginx:1.14.1-alpine
## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/
## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ng-app/dist/ /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
