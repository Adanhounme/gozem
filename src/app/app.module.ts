import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FedaPayCheckoutModule } from 'fedapay-angular';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { fonction } from './provider/fedapay/const/methode';
import { IdentificationComponent } from './identification/identification.component';
import { MapsComponent } from './maps/maps.component';
import { BarreFiltreComponent } from './barre-filtre/barre-filtre.component';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    IdentificationComponent,
    MapsComponent,
    BarreFiltreComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    LeafletModule,
    HttpClientModule,
    FedaPayCheckoutModule.forRoot({ public_key: 'pk_sandbox_Il7n4ef-EEegXc_0PLe_1Km5' }),
    AppRoutingModule,
    
  ],
  providers: [fonction],
  bootstrap: [AppComponent]
})
export class AppModule { }
